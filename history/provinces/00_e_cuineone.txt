#k_curiers
##d_rialles
###c_corselier
1 = {	#Lousteau
	culture = curienne
	religion = regelas
	holding = castle_holding
}

2 = {	#Pirtiera
	holding = city_holding
}

3 = {	#Palenes
	holding = church_holding
}

5 = {	#Charamage
	holding = none
}

6 = {	#Ainesua
	holding = city_holding
}

7 = {	#Vallegile
	holding = none
}

###c_tes_taroil
4 = {	#Artrecil
	culture = curienne
	religion = regelas
	holding = castle_holding
}

8 = {	#Hoires
	holding = city_holding
}

17 = {	#Asile
	holding = city_holding
}

###c_gralelles
9 = {	#Gralelle
	culture = curienne
	religion = regelas
	holding = castle_holding
}

20 = {	#Acielle
	holding = none
}

21 = {	#Daurelle
	holding = castle_holding
}

###c_erastal
25 = {	#Premiavis
	culture = curienne
	religion = regelas
	holding = castle_holding
}

24 = {	#Artroes
	holding = none
}

26 = {	#Sielauge
	holding = city_holding
}

27 = {	#Arlitoure
	holding = castle_holding
}

###c_neuchele
18 = {	#Telogois
	culture = curienne
	religion = regelas
	holding = castle_holding
}

19 = {	#Ceulege
	holding = church_holding
}

28 = {	#Tes Armea
	holding = castle_holding
}

###c_coraida
22 = {	#Corsorai
	culture = curienne
	religion = regelas
	holding = castle_holding
}

23 = {	#Sermea
	holding = castle_holding
}

###c_ovroica
14 = {	#Teveren
	culture = curienne
	religion = regelas
	holding = castle_holding
}

10 = {	#Flouia
	holding = none
}

13 = {	#Tistalie
	holding = city_holding
}

15 = {	#Harieul
	holding = none
}

###c_locherne
11 = {	#Avresal
	culture = curienne
	religion = regelas
	holding = castle_holding
}

12 = {	#Celunis
	holding = church_holding
}

16 = {	#Atregeste
	holding = none
}

30 = {	#Curoule
	holding = none
}

##d_bacerie
###c_bacerie
39 = {	#Bacerie
	culture = curienne
	religion = regelas
	holding = castle_holding
}

36 = {	#Taura
	holding = city_holding
}

40 = {	#Peralas
	holding = none
}

42 = {	#Tevoil
	holding = church_holding
}

###c_pirotie
35 = {	#Torusier
	culture = curienne
	religion = regelas
	holding = castle_holding
}

37 = {	#Varaie
	holding = none
}

38 = {	#Terada
	holding = none
}

###c_trecei
34 = {	#Trecei
	culture = curienne
	religion = regelas
	holding = castle_holding
}

33 = {	#Leprone
	holding = church_holding
}

41 = {	#Ociero
	holding = city_holding
}

###c_reialise
32 = {	#Revior
	culture = curienne
	religion = regelas
	holding = castle_holding
}

31 = {	#Grapore
	holding = none
}

###c_nenele
43 = {	#Nenele
	culture = curienne
	religion = regelas
	holding = castle_holding
}

44 = {	#Ombaste
	holding = none
}

45 = {	#Magille
	holding = church_holding
}

46 = {	#Boutour
	holding = castle_holding
}

##d_hirocalle
###c_valeiral
47 = {	#Valeirea
	culture = curienne
	religion = regelas
	holding = castle_holding
}

48 = {	#Sangiara
	holding = church_holding
}

49 = {	#Escutaille
	holding = city_holding
}

50 = {	#Valinage
	holding = none
}

52 = {	#Diaterm
	holding = none
}

###c_urvallange
60 = {	#Urvaloshe
	culture = curienne
	religion = regelas
	holding = castle_holding
}

51 = {	#Brasaile
	holding = city_holding
}

53 = {	#Toslerne
	holding = none
}

59 = {	#Piern
	holding = none
}

###c_gantiallon
56 = {	#Curobours
	culture = curienne
	religion = regelas
	holding = castle_holding
}

54 = {	#Lomiera
	holding = none
}

55 = {	#Callonage
	holding = city_holding
}

###c_loiram
58 = {	#Loiram
	culture = curienne
	religion = regelas
	holding = castle_holding
}

57 = {	#Rusoiscut
	holding = none
}

###c_seirntour
61 = {	#Seirntour
	culture = curienne
	religion = regelas
	holding = castle_holding
}

62 = {	#Meleurasin
	holding = none
}

63 = {	#Setes
	holding = none
}

##d_tesunier
###c_suniora
64 = {	#Suniora
	culture = curienne
	religion = regelas
	holding = castle_holding
}

65 = {	#Courileia
	holding = none
}

66 = {	#Vuthours
	holding = church_holding
}

67 = {	#Sirreage
	holding = city_holding
}

###c_noireute
68 = {	#Noireute
	culture = curienne
	religion = demaroile
	holding = castle_holding
}

69 = {	#Demarsoe
	holding = church_holding
}

70 = {	#Fithoix
	holding = church_holding
}

###c_vouterm
71 = {	#Vouterm
	culture = curienne
	religion = regelas
	holding = castle_holding
}

72 = {	#Hadois
	holding = city_holding
}

73 = {	#Aruadia
	holding = none
}

74 = {	#Voreail
	holding = none
}

##d_gantouret
###c_comiligas
75 = {	#Comiligas
	culture = curienne
	religion = regelas
	holding = castle_holding
}

76 = {	#Cansiotia
	holding = none
}

77 = {	#Palis
	holding = none
}

78 = {	#Vilrite
	holding = church_holding
}

###c_relois
79 = {	#Reporail
	culture = curienne
	religion = regelas
	holding = castle_holding
}

80 = {	#Meluerinoes
	holding = none
}

81 = {	#Reiasil
	holding = church_holding
}

82 = {	#Loisour
	holding = city_holding
}

###c_taigois
83 = {	#Taigois
	culture = curienne
	religion = regelas
	holding = castle_holding
}

84 = {	#Curoute
	holding = none
}

85 = {	#Sumares
	holding = city_holding
}

86 = {	#Rasonori
	holding = none
}

###c_palestoi
87 = {	#Palestoi
	culture = curienne
	religion = demaroile
	holding = castle_holding
}

88 = {	#Nestagila
	holding = none
}

89 = {	#Axerinis
	holding = none
}

###c_coimisum
90 = {	#Coimiseia
	culture = curienne
	religion = demaroile
	holding = castle_holding
}

91 = {	#Vilese
	holding = church_holding
}

92 = {	#Eniustos
	holding = city_holding
}

93 = {	#Riligo
	holding = none
}

###c_taseves
94 = {	#Taseves
	culture = curienne
	religion = regelas
	holding = castle_holding
}

95 = {	#Tornosin
	holding = city_holding
}

96 = {	#Millipra
	holding = none
}

97 = {	#Cominis
	holding = none
}

###c_manteros
98 = {	#Manterum
	culture = curienne
	religion = regelas
	holding = city_holding
}

99 = {	#Nateros
	holding = castle_holding
}

100 = {	#Avaros
	holding = castle_holding
}

###c_vrothego
101 = {	#Vrothego
	culture = curienne
	religion = demaroile
	holding = castle_holding
}

102 = {	#Coratia
	holding = none
}

103 = {	#Periros
	holding = none
}

###c_trogule
104 = {	#Trogule
	culture = curienne
	religion = regelas
	holding = castle_holding
}

105 = {	#Fithigo
	holding = none
}

106 = {	#Saemolite
	holding = none
}

###c_komeste
107 = {	#Krenikos
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

108 = {	#Takome
	holding = none
}

###c_tornothe
109 = {	#Mardisra
	culture = curienne
	religion = regelas
	holding = castle_holding
}

110 = {	#Runose
	holding = none
}

111 = {	#Tulial
	holding = none
}

###c_omesoire
112 = {	#Omesoire
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

113 = {	#Vemsai
	holding = city_holding
}

114 = {	#Armesie
	holding = city_holding
}

115 = {	#Namesa
	holding = city_holding
}

##d_masiaor
###c_masiaor
116 = {	#Masiaor
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

117 = {	#Aspoile
	holding = city_holding
}

118 = {	#Apilois
	holding = city_holding
}

119 = {	#Esteirol
	holding = church_holding
}

120 = {	#Rotavais
	holding = none
}

###c_vacrels
121 = {	#Vacrels
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

122 = {	#Tosaia
	holding = city_holding
}

123 = {	#Goralo
	holding = none
}

###c_aretaios
124 = {	#Aretaios
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

125 = {	#Potosine
	holding = city_holding
}

126 = {	#Mesine
	holding = none
}

###c_migora
127 = {	#Migora
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

128 = {	#Notavos
	holding = city_holding
}

129 = {	#Enaire
	holding = none
}

###c_anatoine
130 = {	#Anatoine
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

131 = {	#Rigeia
	holding = city_holding
}

132 = {	#Oleisa
	holding = church_holding
}

133 = {	#Toleia
	holding = none
}

###c_trataisia
134 = {	#Trataisia
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

135 = {	#Naere
	holding = none
}

136 = {	#Castroive
	holding = castle_holding
}

137 = {	#Risoles
	holding = church_holding
}

##d_pitrase
###c_maccavo
138 = {	#Maccavo
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

139 = {	#Peroto
	holding = none
}

140 = {	#Vacosai
	holding = city_holding
}

141 = {	#Micasia
	holding = city_holding
}

###c_penchos
142 = {	#Penchos
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

143 = {	#Marot
	holding = none
}

144 = {	#Evaros
	holding = city_holding
}

###c_moslea
145 = {	#Moslea
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

146 = {	#Aretis
	holding = none
}

###c_reote
147 = {	#Reote
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

148 = {	#Verasan
	holding = none
}


##d_visaire
###c_serota
149 = {	#Serotei
	culture = visaire
	religion = kyridras
	holding = castle_holding
}

150 = {	#Baintour
	holding = none
}

151 = {	#Netosa
	holding = city_holding
}

152 = {	#Rodua
	holding = church_holding
}

###c_linpier
153 = {	#Lipora
	culture = visaire
	religion = kyridras
	holding = castle_holding
}

154 = {	#Vasere
	holding = city_holding
}

155 = {	#Deroile
	holding = none
}

156 = {	#Cinpais
	holding = none
}

###c_tethere
157 = {	#Tethoras
	culture = visaire
	religion = naxonos
	holding = castle_holding
}

158 = {	#Vustres
	holding = none
}

159 = {	#Ceuleus
	holding = church_holding
}

160 = {	#Aspiel
	holding = church_holding
}

###c_poisale
161 = {	#Poisale
	culture = visaire
	religion = kyridras
	holding = castle_holding
}

162 = {	#Auviera
	holding = church_holding
}

163 = {	#Merrai
	holding = none
}

##d_adoxega
###c_malceron
164 = {	#Ceronoros
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

165 = {	#Novos Atregeus
	holding = none
}

166 = {	#Perosor
	holding = none
}

167 = {	#Aethoram
	holding = church_holding
}

168 = {	#Adoxen
	holding = city_holding
}

###c_avarause
169 = {	#Malleri
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

170 = {	#Rasonause
	holding = none
}

###c_talgause
171 = {	#Trosetia
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

172 = {	#Talgausor
	holding = none
}

###c_croperae
173 = {	#Lipraese
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

174 = {	#Pirope
	holding = none
}

##d_arliourda
###c_vesperale
175 = {	#Vesperea
	culture = curienne
	religion = regelas
	holding = castle_holding
}

176 = {	#Ovirage
	holding = city_holding
}

177 = {	#Vallail
	holding = church_holding
}

178 = {	#Neusorda
	holding = none
}

###c_vastelle
179 = {	#Nobele
	culture = curienne
	religion = regelas
	holding = castle_holding
}

180 = {	#Tevueloe
	holding = none
}

181 = {	#Corrasin
	holding = city_holding
}

###c_erilois
182 = {	#Erilois
	culture = curienne
	religion = regelas
	holding = castle_holding
}

183 = {	#Tarata
	holding = none
}

184 = {	#Sitoira
	holding = church_holding
}

185 = {	#Dirovie
	holding = city_holding
}

###c_renshe
186 = {	#Renshe
	culture = craina
	religion = regelas
	holding = castle_holding
}

187 = {	#Grois
	holding = none
}

###c_vousere
188 = {	#Vousere
	culture = craina
	religion = regelas
	holding = castle_holding
}

189 = {	#Sorioa
	holding = castle_holding
}

190 = {	#Asceuste
	holding = city_holding
}

191 = {	#Vialin
	holding = church_holding
}

###c_curoau
192 = {	#Curoau
	culture = craina
	religion = demaroile
	holding = castle_holding
}

193 = {	#Poiolis
	holding = none
}

194 = {	#Groera
	holding = city_holding
}

###c_melleurn
195 = {	#Presangi
	culture = curienne
	religion = regelas
	holding = castle_holding
}

196 = {	#Meloshe
	holding = church_holding
}

197 = {	#Grasin
	holding = none
}

198 = {	#Merouia
	holding = none
}

#k_vlouciel
##d_droinas
###c_lusaime
199 = {	#Jurautair
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

200 = {	#Ogesse
	holding = church_holding
}

201 = {	#Tisailaige
	holding = city_holding
}

202 = {	#Cinalias
	holding = church_holding
}

203 = {	#Grallon
	holding = none
}

###c_taurien
204 = {	#Asiere
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

205 = {	#Tisau
	holding = church_holding
}

206 = {	#Vilta
	holding = none
}

207 = {	#Mosaire
	holding = none
}

###c_choiras
208 = {	#Choiras
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

209 = {	#Linusa
	holding = city_holding
}

210 = {	#Miliore
	holding = city_holding
}

211 = {	#Marale
	holding = none
}

###c_mariso
212 = {	#Asposie
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

213 = {	#Maciox
	holding = city_holding
}

214 = {	#Civore
	holding = none
}

###c_vousaixe
215 = {	#Vousaixe
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

216 = {	#Pitoure
	holding = church_holding
}

217 = {	#Bourdia
	holding = none
}

218 = {	#Taigeir
	holding = city_holding
}

###c_trobour
219 = {	#Siremore
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

220 = {	#Vanersoux
	holding = city_holding
}

221 = {	#Orlesie
	holding = none
}

##d_oleisoite
###c_keauxe
222 = {	#Keauxe
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

223 = {	#Alposoir
	holding = city_holding
}

224 = {	#Noteis
	holding = none
}

225 = {	#Oxaine
	holding = city_holding
}

226 = {	#Talekosa
	holding = church_holding
}

###c_ereiaune
227 = {	#Ereiaune
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

228 = {	#Tavois
	holding = church_holding
}

229 = {	#Conoles
	holding = none
}

###c_catolouce
230 = {	#Catolouce
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

231 = {	#Crysossi
	holding = church_holding
}

232 = {	#Seloi
	holding = none
}

233 = {	#Peroise
	holding = castle_holding
}

###c_varois
234 = {	#Varois
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

235 = {	#Loxoete
	holding = none
}

236 = {	#Dorsoule
	holding = none
}


##d_mavouse
###c_sotoise
237 = {	#Sotoise
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

238 = {	#Protail
	holding = city_holding
}

239 = {	#Meragile
	holding = church_holding
}

240 = {	#Muciore
	holding = none
}

###c_potoite
241 = {	#Inperian
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

242 = {	#Mardise
	holding = church_holding
}

243 = {	#Povia
	holding = city_holding
}

###c_taime
244 = {	#Curoura
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

245 = {	#Cosuitte
	holding = city_holding
}

246 = {	#Parose
	holding = none
}

###c_tessoe
247 = {	#Erassoe
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

248 = {	#Vhessoe
	holding = none
}

###c_vithia
249 = {	#Vithia
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

250 = {	#Vuthorre
	holding = castle_holding
}

251 = {	#Paleus Vithoir
	holding = none
}

252 = {	#Morsone
	holding = city_holding
}

##d_tadours
###c_putiloe
253 = {	#Putiloe
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

254 = {	#Gemes
	holding = church_holding
}

255 = {	#Doase
	holding = none
}

256 = {	#Drote
	holding = none
}

257 = {	#Metaure
	holding = city_holding
}

###c_avossor
258 = {	#Avossor
	culture = palecios
	religion = regelas
	holding = castle_holding
}

259 = {	#Varose
	holding = city_holding
}

260 = {	#Toside
	holding = church_holding
}

261 = {	#Migoure
	holding = none
}

###c_dourois
262 = {	#Dourile
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

263 = {	#Aporuse
	holding = none
}

264 = {	#Seloto
	holding = none
}

###c_korales
265 = {	#Korales
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

266 = {	#Lapois
	holding = none
}

267 = {	#Chotos
	holding = none
}

##d_voscierre
###c_carolle
279 = {	#Vosceia
	culture = palecios
	religion = demaroile
	holding = castle_holding
}

280 = {	#Oranters
	holding = none
}

281 = {	#Sorelene
	holding = none
}

282 = {	#Melomes
	holding = city_holding
}

283 = {	#Voscias
	holding = city_holding
}

###c_avarosie
272 = {	#Avarosei
	culture = palecios
	religion = demaroile
	holding = castle_holding
}

273 = {	#Vostia
	holding = none
}

274 = {	#Varenoit
	holding = church_holding
}

###c_pilacoe
275 = {	#Pilacoere
	culture = palecios
	religion = demaroile
	holding = castle_holding
}

276 = {	#Vaitoir
	holding = none
}

277 = {	#Curotte
	holding = city_holding
}

278 = {	#Ligoia
	holding = none
}

###c_voscinear
268 = {	#Tarras
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

269 = {	#Coeite
	holding = city_holding
}

270 = {	#Tegie
	holding = church_holding
}

271 = {	#Sortia
	holding = none
}

###c_maguese
284 = {	#Voscies
	culture = vlouse
	religion = demaroile
	holding = church_holding
}

285 = {	#Dilia
	holding = city_holding
}

286 = {	#Naveol
	holding = none
}

287 = {	#Pralite
	holding = castle_holding
}

###c_vacoite
288 = {	#Vacoite
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

289 = {	#Portoi
	holding = none
}

290 = {	#Reutia
	holding = none
}

291 = {	#Turaus
	holding = castle_holding
}

###c_maciese
292 = {	#Maciese
	culture = vlouse
	religion = demaroile
	holding = church_holding
}

293 = {	#Sateria
	holding = none
}

294 = {	#Robouse
	holding = city_holding
}

###c_verate
297 = {	#Dratii
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

296 = {	#Novate
	holding = city_holding
}

295 = {	#Palata
	holding = none
}

###c_veosci
298 = {	#Veosciea
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

299 = {	#Vaume
	holding = none
}

300 = {	#Rouas
	holding = city_holding
}


##d_sitolie
###c_itroetes
301 = {	#Cinaie
	culture = palecios
	religion = regelas
	holding = castle_holding
}

302 = {	#Cagile
	holding = church_holding
}

303 = {	#Kastrovai
	holding = none
}

304 = {	#Tioras
	holding = city_holding
}

###c_cemeas
305 = {	#Aduane
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

306 = {	#Melerge
	holding = none
}

307 = {	#Aratexe
	holding = city_holding
}

###c_exerros
308 = {	#Exerros
	culture = vlouse
	religion = regelas
	holding = castle_holding
}

309 = {	#Terota
	holding = none
}

310 = {	#Roste
	holding = none
}

###c_corrat
311 = {	#Curos
	culture = vlouse
	religion = regelas
	holding = castle_holding
}

312 = {	#Surope
	holding = city_holding
}

313 = {	#Rovo
	holding = none
}

###c_comore
314 = {	#Sosite
	culture = vlouse
	religion = regelas
	holding = castle_holding
}

315 = {	#Catiosa
	holding = city_holding
}

316 = {	#Atheixe
	holding = none
}

##d_noitire
###c_noitire
317 = {	#Noitire
	culture = masiaorois
	religion = naxonos
	holding = castle_holding
}

318 = {	#Cathoto
	holding = church_holding
}

319 = {	#Straie
	holding = city_holding
}

###c_katiloe
320 = {	#Trethun
	culture = vlouse
	religion = regelas
	holding = castle_holding
}

321 = {	#Claroto
	holding = none
}

322 = {	#Talecois
	holding = none
}

###c_asegois
323 = {	#Futharne
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

324 = {	#Vorocos
	holding = none
}

325 = {	#Torose
	holding = city_holding
}

##d_aitheux
###c_aithois
326 = {	#Pilteres
	culture = palecios
	religion = demaroile
	holding = castle_holding
}

327 = {	#Aithes
	holding = church_holding
}

328 = {	#Keare
	holding = castle_holding
}

329 = {	#Taube
	holding = city_holding
}

###c_casidigo
330 = {	#Casidore
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

331 = {	#Casidia
	holding = city_holding
}

332 = {	#Contome
	holding = none
}

333 = {	#Aithiour
	holding = none
}

###c_valteis
334 = {	#Valeus
	culture = palecios
	religion = demaroile
	holding = castle_holding
}

335 = {	#Veras
	holding = city_holding
}

336 = {	#Vulla
	holding = city_holding
}

337 = {	#Norome
	holding = none
}

338 = {	#Elua
	holding = church_holding
}

###c_ducors
339 = {	#Duire
	culture = vlouse
	religion = demaroile
	holding = castle_holding
}

340 = {	#Ciraia
	holding = church_holding
}

341 = {	#Paile
	holding = none
}

###c_soerale
342 = {	#Soerale
	culture = palecios
	religion = kyridras
	holding = castle_holding
}

343 = {	#Trosos
	holding = church_holding
}

344 = {	#Melroe
	holding = none
}

###c_varotia
345 = {	#Varotia
	culture = palecios
	religion = demaroile
	holding = castle_holding
}

#k_gelange
##d_serchel
###c_coerlange
346 = {	#Virbe des Prince
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

347 = {	#Reaite
	holding = castle_holding
}

348 = {	#Vialeau
	holding = city_holding
}

349 = {	#Soirve
	holding = city_holding
}

350 = {	#Pierapore
	holding = church_holding
}

351 = {	#Naishe
	holding = church_holding
}

352 = {	#Cibasele
	holding = none
}

353 = {	#Grocil
	holding = none
}

###c_aigiloe
354 = {	#Aigilaille
	culture = gelenis
	religion = regelas
	holding = church_holding
}

355 = {	#Aigiloshe
	holding = castle_holding
}

356 = {	#Croviage
	holding = city_holding
}

357 = {	#Tumeroia
	holding = church_holding
}

358 = {	#Grausore
	holding = none
}

###c_crofourde
359 = {	#Gartofe
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

360 = {	#Setesoe
	holding = church_holding
}

361 = {	#Gantouia
	holding = none
}

362 = {	#Viourda
	holding = city_holding
}

###c_curotole
363 = {	#Princares
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

364 = {	#Reixtior
	holding = church_holding
}

365 = {	#Renage
	holding = city_holding
}

366 = {	#Sutior
	holding = none
}

###c_camtelier
367 = {	#Tour des Camte
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

368 = {	#Camteline
	holding = city_holding
}

369 = {	#Sutelier
	holding = city_holding
}

370 = {	#Curoem
	holding = castle_holding
}

###c_sugarte
371 = {	#Sugarte
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

372 = {	#Ceulaille
	holding = church_holding
}

373 = {	#Sutouge
	holding = city_holding
}

374 = {	#Maugesour
	holding = castle_holding
}

###c_espereuroes
375 = {	#Espereuroes
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

376 = {	#Tethelofe
	holding = church_holding
}

377 = {	#Norouge
	holding = city_holding
}

378 = {	#Vrolleil
	holding = none
}

##d_vourde
###c_aulgetaine
379 = {	#Aulgetaine
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

380 = {	#Neus Coter
	holding = city_holding
}

381 = {	#Vouchare
	holding = city_holding
}

382 = {	#Portour
	holding = castle_holding
}

383 = {	#Pirtecil
	holding = none
}

###c_vousiera
384 = {	#Renera
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

385 = {	#Cibiera
	holding = castle_holding
}

386 = {	#Terliera
	holding = none
}

387 = {	#Patiera
	holding = castle_holding
}

###c_cibouse
388 = {	#Courime
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

389 = {	#Cibase Couriage
	holding = city_holding
}

390 = {	#Naecoie
	holding = church_holding
}

391 = {	#Granaec
	holding = none
}

##d_vroloire
###c_neus_vior
392 = {	#Neus Vior
	culture = craina
	religion = demaroile
	holding = castle_holding
}

393 = {	#Craise
	holding = church_holding
}

394 = {	#Ombriele
	holding = none
}

395 = {	#Oiterm
	holding = none
}

###c_aigivel
396 = {	#Aigivel
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

397 = {	#Premiourda
	holding = none
}

398 = {	#Curia
	holding = city_holding
}

###c_pieria
399 = {	#Ceulia
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

400 = {	#Pirtia
	holding = church_holding
}

##d_aigilourda
###c_cerloselle
401 = {	#Sangialaite
	culture = craina
	religion = regelas
	holding = castle_holding
}

402 = {	#Aigilur
	holding = none
}

403 = {	#Tesior
	holding = city_holding
}

404 = {	#Harinour
	holding = church_holding
}

405 = {	#Melaram
	holding = none
}

###c_noireile
406 = {	#Noireile
	culture = craina
	religion = demaroile
	holding = castle_holding
}

407 = {	#Seteude
	holding = city_holding
}

408 = {	#Toirm
	holding = none
}

###c_sutores
409 = {	#Sutort
	culture = craina
	religion = demaroile
	holding = castle_holding
}

410 = {	#Rusaine
	holding = none
}

411 = {	#Atriera
	holding = church_holding
}

###c_voutole
412 = {	#Voutole
	culture = craina
	religion = demaroile
	holding = castle_holding
}

413 = {	#Reist
	holding = none
}

414 = {	#Reiude
	holding = city_holding
}

###c_patoira
415 = {	#Seuntour
	culture = craina
	religion = regelas
	holding = castle_holding
}

416 = {	#Aigilore
	holding = castle_holding
}

417 = {	#Dialier
	holding = none
}

###c_tesor
418 = {	#Torea
	culture = craina
	religion = regelas
	holding = castle_holding
}

419 = {	#Cupore
	holding = city_holding
}

420 = {	#Valige
	holding = city_holding
}

##d_teirone
###c_caulerete
421 = {	#Delunaive
	culture = craina
	religion = demaroile
	holding = castle_holding
}

422 = {	#Cotauria
	holding = none
}

423 = {	#Verlaravi
	holding = city_holding
}

424 = {	#Navreci
	holding = none
}

425 = {	#Vialeset
	holding = church_holding
}

426 = {	#Moramois
	holding = none
}

###c_pieragile
427 = {	#Pieragile
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

428 = {	#Vhesite
	holding = none
}

429 = {	#Tumeia
	holding = none
}

###c_bosouia
430 = {	#Bosirage
	culture = craina
	religion = regelas
	holding = castle_holding
}

431 = {	#Ailollon
	holding = city_holding
}

432 = {	#Nobaille
	holding = church_holding
}

433 = {	#Curois
	holding = none
}

###c_choime
434 = {	#Choime
	culture = craina
	religion = demaroile
	holding = castle_holding
}

435 = {	#Eraset
	holding = church_holding
}

436 = {	#Verlasoer
	holding = none
}

437 = {	#Scilleoir
	holding = none
}

###c_curouaul
438 = {	#Curouaul
	culture = craina
	religion = demaroile
	holding = castle_holding
}

439 = {	#Norteau
	holding = none
}

440 = {	#Tes Moneite
	holding = church_holding
}

###c_cipioran
441 = {	#Cipioran
	culture = craina
	religion = regelas
	holding = castle_holding
}

442 = {	#Cilunes
	holding = city_holding
}

443 = {	#Uveort
	holding = city_holding
}

444 = {	#Rusonte
	holding = none
}

###c_ceisetes
445 = {	#Ceisetes
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

446 = {	#Nailetour
	holding = none
}

447 = {	#Merescut
	holding = city_holding
}

##d_meiveouia
###c_tes_mirsoen
448 = {	#Demarares
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

449 = {	#Corsarin
	holding = city_holding
}

450 = {	#Tirelause
	holding = none
}

451 = {	#Acairause
	holding = none
}

###c_jehansaive
452 = {	#Jehansaive
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

453 = {	#Viraguse
	holding = none
}

454 = {	#Milleveren
	holding = none
}

###c_verlalonga
455 = {	#Pirtelange
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

456 = {	#Faerivalles
	holding = none
}

457 = {	#Pierachele
	holding = none
}

###c_sutuvon
458 = {	#Treilcallon
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

459 = {	#Curollas
	holding = none
}

460 = {	#Haurtuvon
	holding = city_holding
}

461 = {	#Noiretreil
	holding = none
}

###c_noruvon
462 = {	#Aspallon
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

463 = {	#Meiveon
	holding = none
}

464 = {	#Camtecoives
	holding = none
}

465 = {	#Vastharam
	holding = none
}

###c_contirel
466 = {	#Roucharam
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

467 = {	#Cuineiros
	holding = city_holding
}

468 = {	#Heriflos
	holding = none
}

##d_cairslaige
###c_naredaer
469 = {	#Tes Fegel
	culture = cairslaige
	religion = regelas
	holding = castle_holding
}

470 = {	#Getainaer
	holding = city_holding
}

471 = {	#Gastanai
	holding = city_holding
}

472 = {	#Mefiraige
	holding = none
}

473 = {	#Paerine
	holding = church_holding
}

474 = {	#Mefote
	holding = none
}

###c_sotastele
475 = {	#Feaumasel
	culture = cairslaige
	religion = regelas
	holding = castle_holding
}

476 = {	#Fralarua
	holding = city_holding
}

477 = {	#Meleteu
	holding = church_holding
}

478 = {	#Foregel
	holding = none
}

###c_piramasel
479 = {	#Faretoer
	culture = cairslaige
	religion = regelas
	holding = castle_holding
}

480 = {	#Fesorte
	holding = none
}

481 = {	#Braecaer
	holding = church_holding
}

482 = {	#Ganteset
	holding = none
}

###c_aritael
483 = {	#Jossetamer
	culture = cairslaige
	religion = regelas
	holding = castle_holding
}

484 = {	#Getairaem
	holding = city_holding
}

485 = {	#Tiramer
	holding = none
}

486 = {	#Sibasare
	holding = none
}

###c_faletem
487 = {	#Naretere
	culture = cairslaige
	religion = regelas
	holding = castle_holding
}

488 = {	#Miliset
	holding = church_holding
}

489 = {	#Lang da Fortir
	holding = none
}

490 = {	#Corofale
	holding = none
}

###c_sotalos
491 = {	#Espateis
	culture = cairslaige
	religion = regelas
	holding = castle_holding
}

492 = {	#Pateire
	holding = none
}

493 = {	#Farala
	holding = none
}

##d_espereus
###c_falasie
494 = {	#Traenere
	culture = cairslaige
	religion = regelas
	holding = castle_holding
}

495 = {	#Artresoi
	holding = church_holding
}

496 = {	#Nairete
	holding = city_holding
}

497 = {	#Sebamie
	holding = none
}

498 = {	#Serfere
	holding = castle_holding
}

###c_noudose
499 = {	#Pirsaive
	culture = cairslaige
	religion = regelas
	holding = castle_holding
}

500 = {	#Naerose
	holding = none
}

501 = {	#Sotaose
	holding = none
}

###c_leitroein
502 = {	#Forafie
	culture = cairslaige
	religion = regelas
	holding = castle_holding
}

503 = {	#Grelois
	holding = church_holding
}

504 = {	#Amarsiare
	holding = city_holding
}

505 = {	#Meremie
	holding = none
}

506 = {	#Getainosel
	holding = none
}

###c_cafiore
507 = {	#Saenctoir
	culture = cairslaige
	religion = regelas
	holding = castle_holding
}

508 = {	#Getainor
	holding = city_holding
}

509 = {	#Taeroie
	holding = none
}

510 = {	#Nairese
	holding = city_holding
}

###c_borecele
511 = {	#Mirsene
	culture = cairslaige
	religion = regelas
	holding = castle_holding
}

512 = {	#Fantoen
	holding = none
}

513 = {	#Fordais
	holding = city_holding
}

##d_maugestale
###c_poiletour
514 = {	#Mardemar
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

515 = {	#Vasairile
	holding = city_holding
}

516 = {	#Maialois
	holding = city_holding
}

517 = {	#Evrois
	holding = city_holding
}

###c_cavoter
518 = {	#Mavetoir
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

519 = {	#Sandeveau
	holding = none
}

520 = {	#Vroliavole
	holding = church_holding
}

521 = {	#Comera
	holding = city_holding
}

###c_vonsaine
522 = {	#Claurmar
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

523 = {	#Sataira
	holding = city_holding
}

524 = {	#Serrotere
	holding = none
}

###c_ausiledom
525 = {	#Cothuco
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

526 = {	#Cadeverl
	holding = none
}

527 = {	#Pofithae
	holding = church_holding
}

528 = {	#Atelaite
	holding = church_holding
}

##d_cate_ser
###c_garcoter
529 = {	#Garteratein
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

530 = {	#Escutport
	holding = city_holding
}

531 = {	#Caviarve
	holding = castle_holding
}

532 = {	#Argorespe
	holding = castle_holding
}

533 = {	#Flestoie
	holding = none
}

###c_vhesnase
534 = {	#Deretser
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

535 = {	#Langevirage
	holding = city_holding
}

536 = {	#Mucieterm
	holding = castle_holding
}

537 = {	#Estarvior
	holding = church_holding
}

###c_sercamte
538 = {	#Faeris Floes
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

539 = {	#Autretoerse
	holding = none
}

540 = {	#Bernitreler
	holding = city_holding
}

541 = {	#Ornesvirage
	holding = city_holding
}

###c_corslange
542 = {	#Acrovimocil
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

543 = {	#Erastcharam
	holding = none
}

544 = {	#Mucietirel
	holding = none
}

545 = {	#Maugesacrove
	holding = city_holding
}

546 = {	#Vhescharam
	holding = none
}

###c_pelierdrouia
547 = {	#Roshemite
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

548 = {	#Tes Pelier
	holding = none
}

549 = {	#Ornesause
	holding = city_holding
}

###c_tinetoir
550 = {	#Tougedrouia
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

551 = {	#Cibasarua
	holding = none
}

552 = {	#Gralamares
	holding = none
}

##d_princoives
###c_vuorors
553 = {	#Claurithes
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

554 = {	#Alcharam
	holding = church_holding
}

555 = {	#Nobisang
	holding = city_holding
}

556 = {	#Ainaec
	holding = church_holding
}

557 = {	#Uorise
	holding = none
}

###c_treiluvon
558 = {	#Sangoives
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

559 = {	#Coretes
	holding = city_holding
}

560 = {	#Bernea
	holding = church_holding
}

561 = {	#Taroren
	holding = none
}

###c_vrolares
562 = {	#Arlegile
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

563 = {	#Trasouia
	holding = none
}

564 = {	#Amatior
	holding = none
}

565 = {	#Ceulecimme
	holding = city_holding
}

566 = {	#Valesoe
	holding = city_holding
}

###c_auveause
567 = {	#Ceuhele
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

568 = {	#Vuoralaite
	holding = city_holding
}

569 = {	#Coivone
	holding = none
}

###c_curorin
570 = {	#Trebeset
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

571 = {	#Coivenaec
	holding = none
}

572 = {	#Sunallon
	holding = none
}

##d_cibouia
###c_estavois
573 = {	#Pierhavis
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

574 = {	#Vanour
	holding = castle_holding
}

575 = {	#Brasetes
	holding = none
}

576 = {	#Grocore
	holding = none
}

###c_setebeset
577 = {	#Vriosaive
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

578 = {	#Oilange
	holding = none
}

579 = {	#Viasang
	holding = none
}

###c_iocharam
580 = {	#Reixage
	culture = gelenis
	religion = regelas
	holding = castle_holding
}

581 = {	#Couruia
	holding = none
}